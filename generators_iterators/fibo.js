// fibonacci using generators
// scales like hell!
function fiboGenerator(n) {
    const fibo = function*() {
      let [prev, curr] = [0, 1];
      yield prev;
      yield curr;
      while (true) {
        [prev, curr] = [curr, curr + prev];
        yield curr;
      }
    };
    let series = []
    for (const iterator of fibo()) {
      if (iterator > n) break;
      console.log(iterator);
    }
    return fibo
  }
  const fiboGen = fiboGenerator(5)
  fiboGen()