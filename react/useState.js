
// this is my own implementation of useState
//
function useState(stateValue) {
  this.state = stateValue;
  this.setState = (state) => {
    this.state = state;
    return this.state;
  };
  return [this.componentState, this.setState];
}
function React() {
  this.render = () => {
    console.log(this.state);
  };
}
React.prototype.useState = useState;

let component1 = new React();
let [name, setName] = component1.useState({ name: "chetan" });

component1.render();

setName({ name: "chetan kumar" });
component1.render();
